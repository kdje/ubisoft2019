﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Game _game;
    public CharacterController2D _controller;
    public Animator _animator;

    public float runSpeed = 40f;

    float horizontalMove = 0f;
    bool jump = false;
    bool crouch = false;

    private void Awake()
    {
        _currentAngle = 0f;
    }
    // Update is called once per frame
    void Update()
    {
        Rotate();
        //if (_rotating) return;

        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;
        //Debug.Log("PlayerMovement horizmove = " + horizontalMove);
        if (Input.GetButtonDown("Jump"))
            jump = true;
        //Debug.Log("Jump = " + jump);
        _animator.SetInteger("horizontal", horizontalMove > 0 ? 1 : (horizontalMove < 0 ? -1 : 0));
        if (Input.GetButtonDown("Crouch"))
            crouch = true;
        else if (Input.GetButtonUp("Crouch"))
            crouch = false;
        //Debug.Log("crouch = " + crouch);
    }
    #region rotation
    float _currentAngle;
    public bool _rotating = false;
    void Rotate()
    {
        float newAngle = (_game.Angle + 360f) % 360f;
        float currAngle = (_currentAngle + 360f) % 360f;
        if (currAngle == newAngle)
        {
            transform.rotation = Quaternion.Euler(new Vector3(0f, currAngle, 0f));
            _rotating = false;
        }
        else
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, newAngle, 0), 8 * Time.deltaTime);
            _currentAngle = Mathf.Round(transform.rotation.eulerAngles.y);
            if (Mathf.Abs(currAngle - newAngle) < 3f) _currentAngle = newAngle;
            _rotating = true;
        }
    }
    #endregion
    void FixedUpdate()
    {
        //if (_rotating) return;
        // Move our character
        _controller.Move(horizontalMove * Time.fixedDeltaTime, crouch, jump);
        jump = false;
    }
}