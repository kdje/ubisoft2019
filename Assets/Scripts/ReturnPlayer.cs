﻿using UnityEngine;


public class ReturnPlayer : MonoBehaviour
{
    public Transform ReturnPoint;


    public void OnCollisionEnter(Collision other)
    {
        other.transform.position = ReturnPoint.position;
    }
}